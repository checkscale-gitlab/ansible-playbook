ansible==2.10.3
docker==4.3.1
docker-compose==1.27.4
pyOpenSSL==19.1.0
pywinrm==0.4.1
